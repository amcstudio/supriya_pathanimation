'use strict';
~ function() {
  var
  easeInOut = Power1.easeInOut,
  tl = new TimelineMax();
  
    window.init = function(){
      tl.to('#rect',1.5,{strokeDashoffset:400})
      tl.add(drawVerticalLines,'2')
      tl.add(drawhorizontalLines,'2')
      tl.to('#outerCircle',1,{strokeDashoffset:400},'+=1.5')
      tl.to('#innerCircle',1,{strokeDashoffset:400})
      tl.to('#box',1, {rotation:45, ease:easeInOut})
      tl.to(['#outerCircle','#innerCircle','#vLines','#hLines','#box'],2.5,{opacity:0,ease:easeInOut})
      tl.to('.outerPath',1 ,{strokeDashoffset:400},'-=2.5')
      tl.to('.innerPath',1, {strokeDashoffset:400})
      tl.set('.innerPath',{fill:'#ffffff'})
      tl.to('.outerPath',1,{fill:'#00d382'})
    
      
    }
    
function drawVerticalLines(){
  var line;
  var tl = new TimelineMax();
  for(var i=0; i<= 2; i++){
    line = document.createElement('div');
    line.id='line'+i;
    line.className='drawLine';
    line.style.left = 44*i +'px';
    document.getElementById('vLines').appendChild(line);
    tl.to(line,0.5,{height:167},'-=0.2');
  }
}

function drawhorizontalLines(){
  var linee;
  var t2 = new TimelineMax();
  for(var i=0;i<=2;i++){
    linee = document.createElement('div');
    linee.id='hline'+i;
    linee.className='drawhLine';
    linee.style.top = 44*i +'px';
    document.getElementById('hLines').appendChild(linee);

  t2.to(linee,0.5,{width:167},'-=0.2');
  }
}

}();


